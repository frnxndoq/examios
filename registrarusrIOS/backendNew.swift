//
//  backendNew.swift
//  registrarusrIOS
//
//  Created by Fernando on 14/6/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import Foundation


class BackendPelis {
    let serie:String
    let country:String
    let network:String
    let status:String
    let imgurl:String
    
    init(serie:String, country:String, network:String, status:String, imgurl:String){
        self.serie = serie
        self.country = country
        self.network = network
        self.status = status
        self.imgurl = imgurl
    }
    
}
