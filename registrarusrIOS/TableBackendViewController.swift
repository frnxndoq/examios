//
//  TableBackendViewController.swift
//  registrarusrIOS
//
//  Created by Fernando on 14/6/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import UIKit

class TableBackendViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    
    @IBOutlet weak var tablabackend: UITableView!
    
    var selectPaht = IndexPath ()
    var seriesback  = [BackendPelis]()
    var TableData:Array< String > = Array < String >()
    var selectedIndexPath = IndexPath()
    let bm = BackendImages()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizar_backend"), object: nil)
        
    }
    
    func actualizarInformacion(_ notification: Notification){
        seriesback = notification.userInfo?["listo_backend"] as! [BackendPelis]
        tablabackend.reloadData()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar_backend"),object:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let backend = TabBarViewController()
        backend.consultarBackend()
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return seriesback.count
        
        
    }
    
    //esta funcion me tiene que devolver UItableviewcwll
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellbackend") as! BackendTableViewCell
        
        cell.serie_back.text = seriesback[indexPath.row].serie ?? "No"
        cell.country_back.text = seriesback[indexPath.row].country ?? "No"
        cell.network_back.text = seriesback[indexPath.row].network ?? "No"
        cell.state_back.text = seriesback[indexPath.row].status ?? "No"
        
        /*if let pelImage = peliImages[indexPath.row]
         {
         cell.img_usuario.image = pelImage
         }
         else{
         bm.downloadImagepelis(indexPath.row, completionMandler: { (image)
         in
         DispatchQueue.main.async {
         cell.img_usuario?.image = image
         tableView.reloadData()
         }
         })
         }*/
        
        if let pelisimgback = peliImagesBackend[indexPath.row]
        {
            cell.img_back.image = pelisimgback
        }
        else{
            
            bm.downloadImagesBackend(seriesback[indexPath.row].imgurl, i: indexPath.row, completionMandler: { (image)
                in
                DispatchQueue.main.async {
                    
                    cell.img_back?.image = image
                    
                    tableView.reloadData()
                    
                }
                
            })
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectPaht = indexPath
        return indexPath
        
    }

}
