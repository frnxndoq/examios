//
//  BackendTableViewCell.swift
//  registrarusrIOS
//
//  Created by Fernando on 14/6/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import UIKit

class BackendTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var img_back: UIImageView!
    @IBOutlet weak var serie_back: UILabel!
    @IBOutlet weak var country_back: UILabel!
    @IBOutlet weak var network_back: UILabel!
    @IBOutlet weak var state_back: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
