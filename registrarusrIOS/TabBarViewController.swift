//
//  TabBarViewController.swift
//  registrarusrIOS
//
//  Created by Lilian Quimbita on 6/8/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    func  solicitarBackEnd(urlString:String){
        Alamofire.request(urlString).responseJSON { response in
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                
            }
            
        }
        
    }
    //https://www.episodate.com/api
    func consultar()
    {
        let urlString = "https://www.episodate.com/api/most-popular?page=1"
      
        Alamofire.request(urlString).responseJSON { response in
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                let jsonDict = JSON as! NSDictionary
                let json1 = jsonDict["tv_shows"] as! NSArray
                var array = [Persona]()
                
                for i in json1 {
                    let dict = i as! NSDictionary
                    
                    array.append(Persona(serie:dict["name"]! as! String,
                                    country:dict["country"]! as! String,
                                    network:dict["network"]! as! String,
                                    status:dict["status"]! as! String,
                                    imgurl:dict["image_thumbnail_path"]! as! String))}
                NotificationCenter.default.post(name: NSNotification.Name("actualizar"), object: nil, userInfo: ["listo":array])
                print("se envio!!")
        }
    }
}
    
 
        func consultarBackend()
        {
            let urlString = "http://localhost:1337/Datos"
            Alamofire.request(urlString).responseJSON { response in
                if let JSON = response.result.value {
                    print("JSON: \(JSON)")
                    let jsonDict = JSON as! NSArray;
                    var array = [BackendPelis]()
                    
                    
                    for i in jsonDict {
                        let dict = i as! NSDictionary
                        
                        array.append(BackendPelis(serie:dict["serie"]! as! String,
                                             country:dict["pais"]! as! String,
                                             network:dict["cadena"]! as! String,
                                             status:dict["estado"]! as! String,
                                             imgurl:dict["imagen"]! as! String))}
                    
                    
                    NotificationCenter.default.post(name: NSNotification.Name("actualizar_backend"), object: nil, userInfo: ["listo_backend":array])
                    
                    print("se envio!!")
                }
                
            }
    }

}
