//
//  Backend.swift
//  registrarusrIOS
//
//  Created by Lilian Quimbita on 6/8/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import Foundation


class Persona {
    let serie:String
    let country:String
    let network:String
    let status:String
    let imgurl:String
    
    init(serie:String, country:String, network:String, status:String, imgurl:String){
        self.serie = serie
        self.country = country
        self.network = network
        self.status = status
        self.imgurl = imgurl
    }
    
}

