//
//  ViewController.swift
//  registrarusrIOS
//

import UIKit
import BMSCore
import Alamofire
import CoreLocation
import Foundation

import SwiftCloudant


class ViewController: UIViewController {

    @IBOutlet weak var txt_usuario: UITextField!
    
    @IBOutlet weak var txt_password: UITextField!
    
    @IBOutlet weak var txt_email: UITextField!
    
    let dbName = "registrar"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)

  // còdigo genererado automàticamente
/*
        if let contents = Bundle.main.path(forResource:"BMSCredentials", ofType: "plist"), let dictionary = NSDictionary(contentsOfFile: contents) {
            let url = URL(string: dictionary["cloudantUrl1"] as! String)
            let client = CouchDBClient(url:url!,
                                       username:dictionary["cloudantUsername1"] as? String,
                                       password:dictionary["cloudantPassword1"] as? String)
 
        }*/
    }
    
    func didBecomeActive(_ notification: Notification) {
        
        
    }
    
    @IBAction func ingresar(_ sender: Any) {
        
        let urlString = "http://localhost:1337/Usuario"
        
        let parameters: Parameters = [
            "usuario" : txt_usuario.text ?? "",
            "contraeña" : txt_password.text ?? "",
            "correo" : txt_email.text ?? ""        ]
        
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON{ response in
            
            if let JSON = response.result.value{
                print("JSON: \(JSON)")
            }
            
            
        }
       
    }
    
    
    func obtenerUsuario()
    {
    
        let urlString = "http://localhost:1337/Usuario"
        print(urlString)
      
        
        Alamofire.request(urlString).responseJSON {
            
            response in
            
            if let JSON = response.result.value
            {

                let jsonDic =  JSON as! NSArray
                
                
                NotificationCenter.default.post(name: NSNotification.Name("ACTUALIZAR"),object:nil,userInfo:["Lista" :jsonDic])
        
    
     
         }
            
        }
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //codigo para ocultar el teclado
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        txt_usuario.resignFirstResponder()
        txt_password.resignFirstResponder()
        txt_email.resignFirstResponder()
    }


}

