//
//  BackendImages.swift
//  registrarusrIOS
//
//  Created by Lilian Quimbita on 6/8/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage



class BackendImages {

    

    func downloadImages(_ id:String, i:Int, completionMandler: @escaping(UIImage)-> () )
{

    
    //let URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
    
    let URL = "\(id)"
    
    Alamofire.request(URL).responseImage {
    
        response in
        guard let image = response.result.value else
        {
            return
        }
        peliImages [i] = image
        completionMandler(image)

    }
    
   }
    
    func downloadImagesdetalle(_ id:String, completionMandler: @escaping(UIImage)-> () )
    {
        
        
        //let URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        let URL = "\(id)"
        
        Alamofire.request(URL).responseImage {
            
            response in
            guard let image = response.result.value else
            {
                return
            }
            completionMandler(image)
            
        }
        
    }
    
    
    func downloadImagesBackend(_ id:String, i:Int, completionMandler: @escaping(UIImage)-> () )
    {
        
        
        //let URL = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        let URL = "\(id)"
        
        Alamofire.request(URL).responseImage {
            
            response in
            guard let image = response.result.value else
            {
                return
            }
             peliImagesBackend[i] = image
            completionMandler(image)
            
        }
        
    }


}


