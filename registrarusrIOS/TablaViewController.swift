//
//  TablaViewController.swift
//  registrarusrIOS
//
//  Created by Fernando on 5/6/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import UIKit
import BMSCore

import Alamofire
import SwiftCloudant



class TablaViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tablaUsuario: UITableView!
    var selectPaht = IndexPath ()
    var usuarios  = [Persona]()
    var TableData:Array< String > = Array < String >()
    var selectedIndexPath = IndexPath()
    let bm = BackendImages()
  
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        NotificationCenter.default.addObserver(self, selector:#selector(actualizarInformacion), name: NSNotification.Name("actualizar"), object: nil)
        
    }
    
    func actualizarInformacion(_ notification: Notification){
        usuarios = notification.userInfo?["listo"] as! [Persona]
        tablaUsuario.reloadData()
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("actualizar"),object:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let backend = TabBarViewController()
        backend.consultar()
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return usuarios.count

        
    }
    
    //esta funcion me tiene que devolver UItableviewcwll
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseCell") as! ExerciseTableViewCell

        cell.txt_nombre.text = usuarios[indexPath.row].serie ?? "No"
        
        
        /*if let pelImage = peliImages[indexPath.row]
        {
            cell.img_usuario.image = pelImage
        }
        else{
            bm.downloadImagepelis(indexPath.row, completionMandler: { (image)
                in
                DispatchQueue.main.async {
                    cell.img_usuario?.image = image
                    tableView.reloadData()
                }
            })
        }*/
    
        if let pelisimg = peliImages[indexPath.row]
        {
            cell.img_usuario.image = pelisimg
        }
        else{
            
            bm.downloadImages(usuarios[indexPath.row].imgurl, i: indexPath.row, completionMandler: { (image)
                in
                DispatchQueue.main.async {
                    
                    cell.img_usuario?.image = image
                    
                tableView.reloadData()
                    
                }
            
            })
        
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        selectPaht = indexPath
        return indexPath
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detalleSerie" {
            
            if let segundaVC = segue.destination as? DetailSerieViewController {
                
                segundaVC.infoObject = usuarios[selectPaht.row].imgurl
                segundaVC.infoObjectSerie = usuarios[selectPaht.row].serie
                segundaVC.infoObjectCountry = usuarios[selectPaht.row].country
                segundaVC.infoObjectNetwork = usuarios[selectPaht.row].network
                segundaVC.infoObjectStatus = usuarios[selectPaht.row].status
            
            }
        }
        
    }

}
