//
//  DetailSerieViewController.swift
//  registrarusrIOS
//
//  Created by Fernando on 13/6/17.
//  Copyright © 2017 IBM. All rights reserved.
//

import UIKit
import Alamofire

class DetailSerieViewController: UIViewController {
    var infoObject:String?
    var indiceTabla:Int?
    var infoObjectSerie:String?
    var infoObjectCountry:String?
    var infoObjectNetwork:String?
    var infoObjectStatus:String?
    let bm = BackendImages()
    
    @IBOutlet weak var Serie_label: UILabel!
    @IBOutlet weak var Country_label: UILabel!
    @IBOutlet weak var ImagenPelicula: UIImageView!
    @IBOutlet weak var NombrePelicula: UILabel!
    @IBOutlet weak var Status: UILabel!
    @IBOutlet weak var Network_label: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if infoObject != nil {
            //NombrePelicula.text = infoObject
            Country_label.text = infoObjectCountry
            Serie_label.text = infoObjectSerie
            Network_label.text = infoObjectNetwork
            Status.text = infoObjectStatus
            
        }
        
            bm.downloadImagesdetalle(infoObject!, completionMandler: { (image)
                in
                DispatchQueue.main.async {
                    
                    self.ImagenPelicula.image = image
                    
                }
                
            })
            
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func PedidoButton(_ sender: Any) {
        //let urlString = "http://localhost:1337/Datos/create?serie=\(Serie_label.text!)&pais=\(Country_label.text!)&cadena=\(Network_label.text!)&estado=\(Status.text!)&imagen=\(infoObject!)"
           // print(urlString)
        
        let urlString = "http://localhost:1337/Datos"
        
        let parameters = [
            "serie": Serie_label.text ?? "",
            "pais" : Country_label.text ?? "",
            "cadena" : Network_label.text ?? "",
            "estado" : Status.text ?? "",
            "imagen" : infoObject ?? ""
        ]
        
        Alamofire.request(urlString, method: .post, parameters: parameters).responseJSON{ response in
            
            if let JSON = response.result.value{
                print("JSON: \(JSON)")
            }
            
            
        }
        
//        Alamofire.request(urlString).responseJSON {
//            
//            response in
//            
//            if let JSON = response.result.value
//            {
//                print ("JSON: \(JSON)")
//            }
//        }
        
    }
    

}
